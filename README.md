# Web Echo
Speak into the microphone and send the audio into a speaker, can be a Bluetooth loudspeaker.

# Next steps
- style the page with Tailwind
- PWA just for fun


# Credits

JS code skeleton:
https://stackoverflow.com/a/27852403/4243067

Audio selection:
https://jsfiddle.net/bomzj/beap6n2g

Loudspeaker icon created by Freepik - Flaticon
