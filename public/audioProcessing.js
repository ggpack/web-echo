const audioInputSelect = document.querySelector("select#audioInput")
const inputSelectors = [audioInputSelect]

const audioOutputSelect = document.querySelector("select#audioOutput")
const outputSelectors = [audioOutputSelect]

const volumeSlider = document.getElementById("volume")

const startButton = document.querySelector("#start")

function displayIOs(deviceInfos)
{
	// Handles being called several times to update labels. Preserve the current item.
	/*const values = inputSelectors.map(select => select.value)

	console.log("values", values)

	inputSelectors.forEach(select =>
	{
		while (select.firstChild)
		{
			select.removeChild(select.firstChild)
		}
	})*/

	for(const deviceInfo of deviceInfos)
	{
		const option = document.createElement("option")
		option.value = deviceInfo.deviceId
		if(deviceInfo.kind === "audioinput")
		{
			option.text = deviceInfo.label || `microphone ${audioInputSelect.length + 1}`
			audioInputSelect.appendChild(option)
		}
		else if(deviceInfo.kind === "audiooutput")
		{
			console.log("an output", deviceInfo)
			option.text = deviceInfo.label || `speaker ${audioOutputSelect.length + 1}`
			audioOutputSelect.appendChild(option)
		}
	}

	//console.log(inputSelectors)
	//console.log(outputSelectors)

	/*inputSelectors.forEach((select, selectorIndex) =>
	{
		console.log("select.childNodes", select.childNodes)
		console.log("ff", Array.prototype.slice.call(select.childNodes).some(n => n.value === values[selectorIndex]))
		if (Array.prototype.slice.call(select.childNodes).some(n => n.value === values[selectorIndex]))
		{
			select.value = values[selectorIndex]
		}
	})*/
}

function toSpeaker(stream)
{
	const audioContext = new AudioContext()
	const gainNode = audioContext.createGain()
	gainNode.connect( audioContext.destination )

	const microphone_stream = audioContext.createMediaStreamSource(stream)
	microphone_stream.connect(gainNode)

	volumeSlider.onchange = evt =>
	{
		const currentVolume = evt.target.value
		gainNode.gain.value = currentVolume

		console.log("currentVolume ", currentVolume)
	}
}

let stream

async function start() {
	stop()
	console.log("Start")
	// Second call to getUserMedia() with changed device may cause error, so we need to release stream before changing device

	startButton.classList.add("active")
	const audioInput = audioInputSelect.value
	const constraints = {
		audio: {deviceId: audioInput ? {exact: audioInput} : undefined}
	}

	try {
		stream = await navigator.mediaDevices.getUserMedia(constraints)
		toSpeaker(stream)
	}
	catch(excep) {
		console.log("Start exception: ", excep, excep.message)
	}
}

function stop() {
	console.log("Stop", stream)
	startButton.classList.remove("active")

	stream?.getTracks().forEach(track => track.stop())
	stream = undefined
}


let mouseDown = false
startButton.onmousedown = start
startButton.onmouseup = stop

async function init() {
	try {
		const devices = await navigator.mediaDevices.enumerateDevices()
		displayIOs(devices)
	}
	catch(excep) {
		console.log("Exception: ", excep, excep.message)
	}
}

init()
